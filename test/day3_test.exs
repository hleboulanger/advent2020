defmodule Day3Test do
  use ExUnit.Case
  doctest Day3

  test "convert to map of map" do
    {map, _, _} = Day3.load_input("inputs/day3_example")
    assert %{} = map
    assert %{} = map[0]
  end

  test "count tree phase1" do
    assert 7 == Day3.phase1("inputs/day3_example")
  end

  test "count tree phase2" do
    assert 336 == Day3.phase2("inputs/day3_example")
  end
end
