defmodule Day4Test do
  use ExUnit.Case
  doctest Day4

  test "test example" do
    assert 2 = Day4.phase1("inputs/day4_example")
  end

  test "phase2 invalid example" do
    assert 0 = Day4.phase2("inputs/day4_example_p2_invalid")
  end

  test "phase2 valid example" do
    assert 4 = Day4.phase2("inputs/day4_example_p2_valid")
  end
end
