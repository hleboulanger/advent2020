defmodule DaysTest do
  use ExUnit.Case
  doctest Day1
  doctest Day2
  doctest Day5
  doctest Day6
  doctest Day7
  doctest Day8
  doctest Day9
  doctest Day10
  doctest Day12
  doctest Day12P2
  doctest Day13
  doctest Day14
end
