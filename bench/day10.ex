input = Day10.load_program("inputs/day10")

Benchee.run(
  %{
    "phase1" => fn -> Day10.phase1 end,
    "phase1_preloaded" => fn -> Day10.sub_phase1(input) end,
  },
  memory_time: 2
)

Benchee.run(
  %{
    "phase2" => fn -> Day10.phase2 end,
    "phase2_preloaded" => fn -> Day10.sub_phase2(input) end,
  },
  memory_time: 2
)
