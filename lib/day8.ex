defmodule Day8 do
  @filename "inputs/day8"

  @doc """
  iex> Day8.phase1("inputs/day8_example")
  5

  iex> Day8.phase1()
  1816
  """
  def phase1(filename \\ @filename) do
    {:twice, acc} = load_program(filename) |> execute()
    acc
  end

  @doc """
  iex> Day8.phase2()
  [end: 1149]
  """
  def phase2(filename \\ @filename) do
    code = load_program(filename)

    0..Enum.count(code)
    |> Enum.map(&(permute_and_exec(code, &1)))
    |> Enum.reject(&is_nil/1)
    |> Enum.filter(fn {type, _} -> type == :end end)
  end


  def load_program(filename) do
    File.read!(filename)
    |> String.trim
    |> String.split("\n")
    |> Enum.map(&parse_line/1)
  end

  @doc """
  iex> Day8.parse_line("nop +0")
  %{:ins => :nop, :arg => 0, :count => 0}

  iex> Day8.parse_line("jmp +4")
  %{:ins => :jmp, :arg => 4, :count => 0}
  """
  def parse_line(line) do
    [ins, arg] = String.split(line, " ", parts: 2)
    %{:ins => String.to_atom(ins), :arg => String.to_integer(arg), :count => 0}
  end

  def execute(code, pointer \\ 0, acc \\ 0) do
    ins = Enum.at(code, pointer)

    if pointer < Enum.count(code) do
      case ins do
        %{:count => 1}  -> {:twice, acc}
        %{:ins => :nop} -> execute(update_count(code, pointer), pointer + 1, acc)
        %{:ins => :jmp} -> execute(update_count(code, pointer), pointer + ins[:arg], acc)
        %{:ins => :acc} -> execute(update_count(code, pointer), pointer + 1, acc + ins[:arg])
      end
    else
      {:end, acc}
    end
  end

  def update_count(code, index) do
    updated = Map.update!(Enum.at(code, index), :count, &(&1 + 1))
    List.replace_at(code, index, updated)
  end

  def permute_and_exec(code, index) do
      line = Enum.at(code, index)

      line = case line[:ins] do
        :jmp -> Map.replace!(line, :ins, :nop)
        :nop -> Map.replace!(line, :ins, :jmp)
        _ -> nil
      end

      if line do
        execute(List.replace_at(code, index, line))
      end
    end
end
