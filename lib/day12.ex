defmodule Day12 do
  @filename "inputs/day12"

  def load_program(filename) do
    File.read!(filename)
    |> String.trim()
    |> String.split("\n")
    |> Enum.map(&parse_line/1)
  end

  def parse_line(line) do
    {a, v} = String.split_at(line, 1)

    %{
      :action => a,
      :value => String.to_integer(v)
    }
  end

  @doc """
  iex> Day12.phase1("inputs/day12_example")
  25
  """
  def phase1(filename \\ @filename) do
    {_, {x, y}} =
      load_program(filename)
      |> Enum.reduce({"E", {0, 0}}, &move/2)
      |> IO.inspect()

    abs(x) + abs(y)
  end

  def phase2(filename \\ @filename) do
    {_, {x, y}} =
      load_program(filename)
      |> Enum.reduce({"E", {10, 1}}, &move/2)
      |> IO.inspect()

    abs(x) + abs(y)
  end

  def move(%{action: "F", value: v}, {dir, {x, y}}) do
    case dir do
      "N" -> {dir, {x, y + v}}
      "E" -> {dir, {x + v, y}}
      "S" -> {dir, {x, y - v}}
      "W" -> {dir, {x - v, y}}
    end
  end

  def move(%{action: "N", value: v}, {dir, {x, y}}), do: {dir, {x, y + v}}
  def move(%{action: "E", value: v}, {dir, {x, y}}), do: {dir, {x + v, y}}
  def move(%{action: "S", value: v}, {dir, {x, y}}), do: {dir, {x, y - v}}
  def move(%{action: "W", value: v}, {dir, {x, y}}), do: {dir, {x - v, y}}

  @doc """
  iex> Day12.move(%{action: "R", value: 90}, {"N", {0, 0}})
  {"E", {0,0}}

  iex> Day12.move(%{action: "R", value: 90}, {"E", {0, 0}})
  {"S", {0,0}}

  iex> Day12.move(%{action: "R", value: 180}, {"E", {0, 0}})
  {"W", {0,0}}

  iex> Day12.move(%{action: "R", value: 270}, {"E", {0, 0}})
  {"N", {0,0}}

  iex> Day12.move(%{action: "L", value: 90}, {"N", {0, 0}})
  {"W", {0,0}}
  """

  def move(%{action: "R", value: v}, {dir, {x, y}}) do
    cards = ["N", "E", "S", "W"]

    {_, i} =
      cards
      |> Enum.with_index()
      |> Enum.filter(fn {c, _} -> c == dir end)
      |> Enum.at(0)

    {cards |> Enum.at(rem(div(v, 90) + i, 4)), {x, y}}
  end

  def move(%{action: "L", value: v}, {dir, {x, y}}) do
    cards = ["N", "W", "S", "E"]

    {_, i} =
      cards
      |> Enum.with_index()
      |> Enum.filter(fn {c, _} -> c == dir end)
      |> Enum.at(0)

    {cards |> Enum.at(rem(div(v, 90) + i, 4)), {x, y}}
  end
end
