defmodule Day9 do
  @filename "inputs/day9"

  def load_program(filename) do
    File.read!(filename)
    |> String.trim()
    |> String.split("\n")
    |> Enum.map(&String.to_integer/1)
  end

  @doc """
  iex> Day9.phase1("inputs/day9_example", 5)
  127

  iex> Day9.phase1()
  57195069
  """
  def phase1(filename \\ @filename, size \\ 25) do
    code = load_program(filename)
    process(code, size)
  end

  def process(code, size) do
    p = permutation(Enum.take(code, size))
    v = Enum.at(code, size)

    # IO.inspect({code, p, v}, charlists: :as_lists)

    cond do
      v in p -> process(Enum.drop(code, 1), size)
      true -> v
    end
  end

  @doc """
  iex> Day9.permutation([1, 2, 3])
  [3, 4, 3, 5, 4, 5]
  """
  def permutation(subcode) do
    for x <- subcode, y <- subcode, x != y, do: x + y
  end

  @doc """
  iex> Day9.phase2(Day9.phase1("inputs/day9_example", 5), "inputs/day9_example")
  62

  iex> Day9.phase2(57195069)
  7409241
  """
  def phase2(number, filename \\ @filename) do
    code = load_program(filename)

    {first, last} =
      code
      |> Enum.with_index()
      |> Enum.map(fn {e, i} ->
        Enum.reduce_while(Enum.drop(code, i), 0, fn x, acc ->
          cond do
            acc == number -> {:halt, {e, x}}
            acc < number -> {:cont, acc + x}
            true -> {:halt, :end}
          end
        end)
      end)
      |> Enum.filter(&(&1 != :end))
      |> Enum.at(0)

    {min, max} =
      code
      |> Enum.drop_while(&(first != &1))
      |> Enum.take_while(&(last != &1))
      |> Enum.min_max()

    min + max
  end
end
