defmodule Day10 do
  use Memoize
  @filename "inputs/day10"

  def load_program(filename) do
    File.read!(filename)
    |> String.trim()
    |> String.split("\n")
    |> Enum.map(&String.to_integer/1)
    |> Enum.sort()

  end

  @doc """
  iex> Day10.phase1("inputs/day10_example_1")
  35
  iex> Day10.phase1("inputs/day10_example_2")
  220
  """
  def phase1(filename \\ @filename) do
    sub_phase1(load_program(filename))
  end

  def sub_phase1(sorted) do
    [{_, x}, {_, y}] =
      ([0 | sorted] ++ [Enum.max(sorted) + 3])
      |> Enum.chunk_every(2, 1, :discard)
      |> Enum.map(fn [n1, n2] -> n2 - n1 end)
      |> Enum.group_by(& &1)
      |> Enum.map(fn {k, v} ->
        {k, Enum.count(v)}
      end)

    x * y
  end

  @doc """
  iex> Day10.phase2("inputs/day10_example_1")
  8
  iex> Day10.phase2("inputs/day10_example_2")
  19208
  """
  def phase2(filename \\ @filename) do
    load_program(filename)
    |> sub_phase2
  end

  def sub_phase2(sorted) do
    sorted
    |> combine(0)
  end

  # use Memoize library
  defmemo(combine([], _current), do: 1)

  defmemo combine(list, current) do
    list
    |> Enum.slice(0..2)
    |> Enum.with_index()
    |> Enum.filter(fn {n, _} -> n - current <= 3 end)
    |> Enum.map(fn {n, i} ->
      {n, Enum.slice(list, (i + 1)..-1)}
    end)
    |> Enum.map(fn {n, t} ->
      combine(t, n)
    end)
    |> Enum.sum()
  end
end
