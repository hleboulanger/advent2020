defmodule Day11P2 do
  # inspired by https://rosettacode.org/wiki/Conway%27s_Game_of_Life#Elixir
  # for Conway Game of Life

  @filename "inputs/day11"

  def phase2(filename \\ @filename) do
    {board, size} = load_board(filename)

    # print_board(board, "", size, 0)
    {reason, board} = generate("", size, 100, board, 1)

    case reason do
      :static -> "no movement"
    end
    |> IO.puts()

    board |> Map.values() |> Enum.count(&(&1 == "#"))
  end

  def new_board(n) do
    for x <- 1..n, y <- 1..n, into: %{}, do: {{x, y}, "."}
  end

  def print_board(_, name, _, generation) do
    IO.puts("#{name}: generation #{generation}")

    # Enum.each(1..n, fn y ->
    #   Enum.map(1..n, fn x -> board[{x, y}] end)
    #   |> IO.puts()
    # end)
  end

  def load_board(filename) do
    data =
      File.read!(filename)
      |> String.trim()
      |> String.split()

    size = Enum.count(data)

    board =
      data
      |> Enum.with_index()
      |> Enum.reduce(new_board(size), fn {line, y}, acc ->
        String.graphemes(line)
        |> Enum.with_index()
        |> Enum.reduce(acc, fn {c, x}, acc ->
          %{acc | {x + 1, y + 1} => c}
        end)
      end)

    {board, size}
  end

  @cardinals [{1, 1}, {1, 0}, {0, 1}, {-1, -1}, {-1, 0}, {0, -1}, {-1, 1}, {1, -1}]

  def cell_next_gen(board, x, y, _) do
    seat = board[{x, y}]

    count =
      Enum.reduce(
        @cardinals,
        [],
        fn {shift_x, shift_y}, acc ->
          Stream.cycle([0])
          |> Enum.reduce_while({acc, x, y}, fn _, {acc, x, y} ->
            {new_x, new_y} = {x + shift_x, y + shift_y}

            case Map.get(board, {new_x, new_y}) do
              "." -> {:cont, {acc, new_x, new_y}}
              nil -> {:halt, acc}
              s -> {:halt, [s | acc]}
            end
          end)
        end
      )
      |> Enum.count(&(&1 == "#"))

    cond do
      seat == "." -> "."
      seat == "L" && count == 0 -> "#"
      seat == "#" && count >= 5 -> "L"
      true -> seat
    end
  end

  def evolve(board, n) do
    for x <- 1..n, y <- 1..n, into: %{}, do: {{x, y}, cell_next_gen(board, x, y, n)}
  end


  defp generate(_, _, generations, _, gen) when generations < gen, do: :ok

  defp generate(name, size, generations, board, gen) do
    new = evolve(board, size)
    # print_board(new, name, size, gen)

    cond do
      board == new -> {:static, new}
      true -> generate(name, size, generations, new, gen + 1)
    end
  end
end
