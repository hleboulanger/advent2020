defmodule Day13 do
  @filename "inputs/day13"

  @doc """
  iex> Day13.phase1("inputs/day13_example")
  295
  """
  def phase1(filename \\ @filename) do
    [timestamp, buses] =
      File.read!(filename)
      |> String.trim()
      |> String.split("\n")

    timestamp = String.to_integer(timestamp)

    buses =
      buses |> String.split(",") |> Enum.reject(&(&1 == "x")) |> Enum.map(&String.to_integer/1)

    {bus_id, dt} =
      buses
      |> Enum.reduce([], fn bus, acc ->
        n =
          Stream.iterate(0, &(&1 + bus))
          |> Stream.drop_while(&(&1 < timestamp))
          |> Stream.take(1)
          |> Enum.at(0)

        [{bus, n} | acc]
      end)
      |> Enum.min_by(fn {_, t} ->
        t
      end)

    (dt - timestamp) * bus_id
  end

  def phase2(filename \\ @filename) do
    [_, buses] =
      File.read!(filename)
      |> String.trim()
      |> String.split("\n")

    # oh tiens, des nombres premiers...

    sub_phase2(buses)
  end

  @doc """
  iex> Day13.sub_phase2("7,13,x,x,59,x,31,19")
  1068781

  iex> Day13.sub_phase2("17,x,13,19")
  3417
  """

  def sub_phase2(line) do
    {mods, remainders} =
      line
      |> String.split(",")
      |> Enum.with_index()
      |> Enum.reduce({[], []}, fn {b, i}, {m, r} ->
        if b != "x" do
          bn = String.to_integer(b)
          {[bn | m], [bn - i | r]}
        else
          {m, r}
        end
      end)

    chinese_remainder(mods, remainders)
  end

  def chinese_remainder(n, a) do
    product = product(n)

    rem =
      Enum.zip(n, a)
      |> Enum.reduce(0, fn {i, j}, acc ->
        p = Integer.floor_div(product, i)
        acc + j * Modular.inverse(p, i) * p
      end)

    Integer.mod(rem, product)
  end

  def product(enumerable) do
    Enum.reduce(enumerable, 1, &*/2)
  end
end

defmodule Modular do
  def extended_gcd(a, b) do
    {last_remainder, last_x} = extended_gcd(abs(a), abs(b), 1, 0, 0, 1)
    {last_remainder, last_x * if(a < 0, do: -1, else: 1)}
  end

  defp extended_gcd(last_remainder, 0, last_x, _, _, _), do: {last_remainder, last_x}

  defp extended_gcd(last_remainder, remainder, last_x, x, last_y, y) do
    quotient = div(last_remainder, remainder)
    remainder2 = rem(last_remainder, remainder)
    extended_gcd(remainder, remainder2, x, last_x - quotient * x, y, last_y - quotient * y)
  end

  def inverse(e, et) do
    {g, x} = extended_gcd(e, et)
    if g != 1, do: raise("The maths are broken!")
    rem(x + et, et)
  end
end
