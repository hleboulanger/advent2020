defmodule Day5 do
  @filename "inputs/day5"

  def phase1(filename \\ @filename) do
    load_input(filename)
    |> Enum.map(&parse_boarding/1)
    |> Enum.max()
  end

  def phase2(filename \\ @filename) do
    boardings =
      load_input(filename)
      |> Enum.map(&parse_boarding/1)

    {min, max} = {Enum.min(boardings), Enum.max(boardings)}

    (Enum.to_list(min..max) -- boardings) |> Enum.at(0)
  end

  def load_input(filename) do
    File.stream!(filename)
  end

  @doc """
  So, decoding FBFBBFFRLR reveals that it is the seat at row 44, column 5.
  Every seat also has a unique seat ID: multiply the row by 8, then add the column. In this example, the seat has ID *44 * 8 + 5 = 357*.

  BFFFBBFRRR => 1000110111 => 567

  iex> Day5.parse_boarding("BFFFBBFRRR")
  567
  iex> Day5.parse_boarding("FFFBBBFRRR")
  119
  iex> Day5.parse_boarding("BBFFBBFRLL")
  820
  """
  def parse_boarding(b) do
    b
    |> String.trim()
    |> String.replace(~r/[BR]/, "1")
    |> String.replace(~r/[FL]/, "0")
    |> String.to_integer(2)
  end
end
