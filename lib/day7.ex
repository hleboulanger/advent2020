defmodule Day7 do

  @filename "inputs/day7"

  @doc """
  iex> Day7.phase1("inputs/day7_example")
  4
  iex> Day7.phase1()
  115
  """
  def phase1(filename \\ @filename) do
    File.stream!(filename)
    |> Enum.map(&parse_bags/1)
    |> generate_graph
    |> Graph.reaching_neighbors(["shiny gold"])
    |> Enum.count
  end


  @doc """
  iex> Day7.phase2("inputs/day7_example_p2")
  126
  iex> Day7.phase2()
  1250
  """
  def phase2(filename \\ @filename) do
    File.stream!(filename)
    |> Enum.map(&parse_bags/1)
    |> generate_graph
    |> count_bags("shiny gold")
    |> dec
  end


  def save_graph(filename) do
    g = File.stream!(filename)
    |> Enum.map(&parse_bags/1)
    |> generate_graph

    {:ok, dot} = Graph.to_dot(g)
    File.write!("graph.dot", dot)
  end


  @doc """
  iex> Day7.parse_bags("faded blue bags contain no other bags.")
  {"faded blue", []}

  iex> Day7.parse_bags("shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.")
  {"shiny gold", [{1, "dark olive"}, {2, "vibrant plum"}]}

  iex> Day7.parse_bags("bright white bags contain 1 shiny gold bag.")
  {"bright white", [{1, "shiny gold"}]}
  """
  def parse_bags(line) do
    [bag, content] = line |> String.trim |> String.split(" bags contain ")

    sub_bags = case content do
      "no other bags." -> []
      _ ->
        content |> String.split(", ") |> Enum.map(fn e ->
        [[_, count, bag]] = Regex.scan(~r/([0-9]+) (\w+ \w+)/, e)
        {String.to_integer(count), bag}
      end)
    end

    {bag, sub_bags}
  end


  def generate_graph(lines) do
    Enum.reduce(lines, Graph.new(), fn {bag, sub_bags}, graph ->
      sub_bags
      |> Enum.reduce(graph, (fn {count, sub_bag}, g ->
          Graph.add_edge(g, bag, sub_bag, label: count)
      end))
    end)
  end


  def count_bags(graph, bag_name) do
    graph
    |> Graph.out_edges(bag_name)
    |> Enum.reduce(1, fn %{v2: name, label: count}, acc ->
      acc + count * count_bags(graph, name)
    end)
  end


  def dec(x) do
    x - 1
  end

end
