defmodule Day2 do
  def phase1() do
    File.read!("inputs/day2")
    |> String.split("\n", trim: true)
    |> Enum.map(&password_valid(&1))
    |> Enum.count(&(&1 == true))
  end

  def phase2() do
    File.read!("inputs/day2")
    |> String.split("\n", trim: true)
    |> Enum.map(&new_password_valid(&1))
    |> Enum.count(&(&1 == true))
  end

  @regex ~r/(?<min>[0-9]+)-(?<max>[0-9]+) (?<letter>[a-z]): (?<password>[a-z]+)/

  @doc """
  iex> Day2.password_valid("1-3 a: abcde")
  true

  iex> Day2.password_valid("1-3 b: cdefg")
  false

  iex> Day2.password_valid("2-9 c: ccccccccc")
  true
  """
  def password_valid(line) do
    parsed = Regex.named_captures(@regex, line)

    count =
      parsed["password"]
      |> String.graphemes()
      |> Enum.frequencies()
      |> Map.get(parsed["letter"], 0)

    count <= String.to_integer(parsed["max"]) &&
      count >= String.to_integer(parsed["min"])
  end

  @new_regex ~r/(?<present>[0-9]+)-(?<absent>[0-9]+) (?<letter>[a-z]): (?<password>[a-z]+)/

  @doc """
  iex> Day2.new_password_valid("1-3 a: abcde")
  true

  iex> Day2.new_password_valid("1-3 b: cdefg")
  false

  iex> Day2.new_password_valid("2-9 c: ccccccccc")
  false

  iex> Day2.new_password_valid("15-16 h: jkhhfhhhnhhhhhhhhhh")
  false

  iex> Day2.new_password_valid("3-11 n: nnnnlnnnznvnn")
  true

  iex> Day2.new_password_valid("9-18 b: bbbmxlbbbxbbbbkbxtx")
  true

  iex> Day2.new_password_valid("11-16 w: wlwxwwwwlptwwwwcw")
  false
  """
  def new_password_valid(line) do
    %{"password" => password, "present" => present, "absent" => absent, "letter" => letter} =
      Regex.named_captures(@new_regex, line)

    present_index = String.to_integer(present) - 1
    absent_index = String.to_integer(absent) - 1

    [
      String.at(password, present_index) == letter,
      String.at(password, absent_index) == letter
    ]
    |> Enum.count(&(&1 == true))
    |> Kernel.==(1)
  end
end
