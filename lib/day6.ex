defmodule Day6 do
  @filename "inputs/day6"

  @doc """
  iex> Day6.phase1("inputs/day6_example")
  11
  iex> Day6.phase1()
  6521
  """
  def phase1(filename \\ @filename) do
    File.read!(filename)
    |> String.split("\n\n")
    |> Enum.map(&parse_answers_any/1)
    |> Enum.sum()
  end

  @doc """
  iex> Day6.phase2("inputs/day6_example")
  6
  iex> Day6.phase2()
  3305
  """
  def phase2(filename \\ @filename) do
    File.read!(filename)
    |> String.split("\n\n")
    |> Enum.map(&parse_answers_every/1)
    |> Enum.sum()
  end

  @doc """
  iex> Day6.parse_answers_any("abc")
  3
  iex> Day6.parse_answers_any("a\\nb\\n\\nc")
  3
  iex> Day6.parse_answers_any("ab\\nac")
  3
  iex> Day6.parse_answers_any("a\\na\\na\\na")
  1
  iex> Day6.parse_answers_any("b")
  1
  """
  def parse_answers_any(group) do
    group
    |> String.replace("\n", "")
    |> String.graphemes()
    |> Enum.reduce(MapSet.new(), &MapSet.put(&2, &1))
    |> Enum.count()
  end

  @doc """
  iex> Day6.parse_answers_every("abc")
  3
  iex> Day6.parse_answers_every("a\\nb\\nc")
  0
  iex> Day6.parse_answers_every("ab\\nac")
  1
  iex> Day6.parse_answers_every("a\\na\\na\\na")
  1
  iex> Day6.parse_answers_every("b")
  1
  """
  def parse_answers_every(group) do
    group
    |> String.trim()
    |> String.split("\n")
    |> Enum.map(&parse_answers_every_line/1)
    |> Enum.reduce(&MapSet.intersection(&1, &2))
    |> Enum.count()
  end

  def parse_answers_every_line(line) do
    line
    |> String.graphemes()
    |> Enum.reduce(MapSet.new(), &MapSet.put(&2, &1))
  end
end
