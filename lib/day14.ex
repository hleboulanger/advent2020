defmodule Day14 do
  @filename "inputs/day14"

  @bit_length 36

  @doc """
  iex> Day14.phase1("inputs/day14_example")
  165
  """
  def phase1(filename \\ @filename) do
    {_, mem} =
      File.read!(filename)
      |> String.trim()
      |> String.split("\n")
      |> Enum.map(&parse_line/1)
      |> Enum.reduce({0, %{}}, fn ins, {mask, mem} ->
        case ins do
          %{op: :mask, value: v} ->
            {v, mem}

          %{op: :set, address: a, value: v} ->
            {mask, Map.put(mem, a, apply_mask(mask, v))}
        end
      end)

    mem |> Enum.reduce(0, fn {_, v}, acc -> acc + v end)
  end

  @doc """
  iex> Day14.phase2("inputs/day14_example_p2")
  208
  """
  def phase2(filename \\ @filename) do
    {_, mem} =
      File.read!(filename)
      |> String.trim()
      |> String.split("\n")
      |> Enum.map(&parse_line/1)
      |> Enum.reduce({0, %{}}, fn ins, {mask, mem} ->
        case ins do
          %{op: :mask, value: v} ->
            {v, mem}

          %{op: :set, address: a, value: v} ->
            combined = combine_mask(mask, a)

            mem =
              Enum.reduce(combined, mem, fn c, m ->
                Map.put(m, c, v)
              end)

            {mask, mem}
        end
      end)

    mem |> Enum.reduce(0, fn {_, v}, acc -> acc + v end)
  end

  def parse_line(line) do
    [ins, value] =
      line
      |> String.split(" = ")

    cond do
      String.starts_with?(ins, "mask") ->
        %{op: :mask, value: value}

      true ->
        %{
          op: :set,
          value: String.to_integer(value),
          address: String.slice(ins, 4..-2) |> String.to_integer()
        }
    end
  end

  @doc """
  iex> Day14.apply_mask("XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X", 11)
  73
  """
  def apply_mask(mask, v) do
    v = value_to_binlist(v)

    Enum.zip(String.graphemes(mask), v)
    |> Enum.reduce([], fn {m, b}, acc ->
      case m do
        "X" -> [b | acc]
        "0" -> [0 | acc]
        "1" -> [1 | acc]
      end
    end)
    |> Enum.reverse()
    |> Integer.undigits(2)
  end

  def value_to_binlist(v) do
    vbin = Integer.digits(v, 2)
    padding = Stream.cycle([0]) |> Enum.take(@bit_length - Enum.count(vbin))

    Enum.concat(padding, vbin)
  end

  def combine_mask(mask, address) do
    Enum.zip(value_to_binlist(address), String.graphemes(mask))
    |> Enum.reduce([0], fn
      {_, "X"}, acc -> fork(acc)
      {_, "1"}, acc -> add_bit(acc, 1)
      {1, _}, acc -> add_bit(acc, 1)
      _, acc -> add_bit(acc, 0)
    end)
  end

  def add_bit(acc, bit), do: Enum.map(acc, &(&1 * 2 + bit))
  def fork(acc), do: Enum.flat_map(acc, &[&1 * 2, &1 * 2 + 1])
end
