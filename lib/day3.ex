defmodule Day3 do
  @filename "inputs/day3"

  def phase1(filename \\ @filename, slope \\ {3, 1}) do
    load_input(filename) |> count_trees(slope, {0, 0})
  end

  def phase2(filename \\ @filename) do
    [{1, 1}, {3, 1}, {5, 1}, {7, 1}, {1, 2}]
    |> Enum.map(&phase1(filename, &1))
    |> Enum.reduce(&Kernel.*/2)
  end

  def load_input(filename) do
    m =
      File.read!(filename)
      |> String.split("\n", trim: true)
      |> Enum.map(&String.graphemes/1)
      |> Matrix.from_list()

    {m, Enum.count(m[0]), Enum.count(m)}
  end

  def count_trees(map, slope, coords, acc \\ 0)
  def count_trees({_m, _w, h}, _slope, {_x, y}, acc) when y > h, do: acc

  def count_trees({m, w, h}, {right, down} = slope, {x, y}, acc) do
    ny = y + down
    nx = rem(x + right, w)
    e = m[ny][nx]

    tree_found =
      case e do
        "#" -> 1
        _ -> 0
      end

    count_trees({m, w, h}, slope, {nx, ny}, acc + tree_found)
  end

  def display_matrix(m) do
    Matrix.to_list(m)
    |> Enum.map(&Enum.join(&1, ""))
    |> Enum.join("\n")
    |> IO.puts()
  end
end
