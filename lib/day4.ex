defmodule Day4 do
  @filename "inputs/day4"

  def phase1(filename \\ @filename) do
    load_input(filename)
    |> Enum.map(&parse_line/1)
    |> Enum.map(&passport_valid?/1)
    |> Enum.count(&(&1 == true))
  end

  def phase2(filename \\ @filename) do
    load_input(filename)
    |> Enum.map(&parse_line/1)
    |> Enum.map(fn line -> passport_and_fields_valid?(line) |> Enum.all?(&(&1 == true)) end)
    |> Enum.count(&(&1 == true))
  end

  def load_input(filename) do
    File.read!(filename)
    |> String.trim()
    |> String.split("\n\n")
    |> Enum.map(&String.replace(&1, "\n", " "))
  end

  @doc """
  iex> Day4.parse_line("ecl:gry pid:860033327 eyr:2020 hcl:#fffffd byr:1937 iyr:2017 cid:147 hgt:183cm")
  %{:ecl => "gry", :pid => "860033327", :eyr => "2020", :hcl => "#fffffd", :byr => "1937", :iyr => "2017",:cid => "147", :hgt => "183cm"}
  """
  def parse_line(line) do
    line
    |> String.split(" ")
    |> Enum.map(&String.split(&1, ":"))
    |> Enum.reduce(%{}, fn [k, v], acc ->
      Map.put(acc, String.to_atom(k), v)
    end)
  end

  @doc """
  iex> Day4.passport_valid?(%{:ecl => "gry", :pid => "860033327", :eyr => "2020", :hcl => "#fffffd", :byr => "1937", :iyr => "2017",:cid => "147", :hgt => "183cm"})
  true

  iex> Day4.passport_valid?(%{byr: "1929", cid: "350", ecl: "amb", eyr: "2023", hcl: "#cfa07d", iyr: "2013", pid: "028048884"})
  false

  iex> Day4.passport_valid?(%{byr: "1931",  ecl: "brn",  eyr: "2024",  hcl: "#ae17e1",  hgt: "179cm",  iyr: "2013",  pid: "760753108"})
  true
  """
  def passport_valid?(passport) do
    [:ecl, :pid, :eyr, :hcl, :byr, :iyr, :hgt] |> Enum.all?(&Map.has_key?(passport, &1))
  end

  def passport_and_fields_valid?(passport) do
    fields = passport |> Enum.map(&field_valid(&1))
    [passport_valid?(passport) | fields]
  end

  @doc """

  iex> Day4.field_valid({:byr, "2002"})
  true
  iex> Day4.field_valid({:byr, "2003"})
  false
  iex> Day4.field_valid({:hgt, "60in"})
  true
  iex> Day4.field_valid({:hgt, "190in"})
  false
  iex> Day4.field_valid({:hgt, "190cm"})
  true
  iex> Day4.field_valid({:hgt, "190"})
  false
  iex> Day4.field_valid({:hcl, "#123abc"})
  true
  iex> Day4.field_valid({:hcl, "#123abz"})
  false
  iex> Day4.field_valid({:hcl, "123abc"})
  false
  iex> Day4.field_valid({:ecl, "brn"})
  true
  iex> Day4.field_valid({:ecl, "wat"})
  false
  iex> Day4.field_valid({:pid, "000000001"})
  true
  iex> Day4.field_valid({:pid, "0123456789"})
  false
  """

  def field_valid({:byr, y}) do
    year = String.to_integer(y)
    String.length(y) == 4 && year >= 1920 && year <= 2002
  end

  def field_valid({:iyr, y}) do
    year = String.to_integer(y)
    String.length(y) == 4 && year >= 2010 && year <= 2020
  end

  def field_valid({:eyr, y}) do
    year = String.to_integer(y)
    String.length(y) == 4 && year >= 2020 && year <= 2030
  end

  @height ~r/(?<height>[1-9]?[0-9][0-9])(?<type>(cm|in))/
  def field_valid({:hgt, h}) do
    Regex.match?(@height, h) &&
      Day4.field_valid(
        Regex.named_captures(@height, h)
        |> Map.update("height", nil, &String.to_integer/1)
      )
  end

  def field_valid(%{"height" => h, "type" => "cm"}) do
    h >= 150 && h <= 198
  end

  def field_valid(%{"height" => h, "type" => "in"}) do
    h >= 59 && h <= 76
  end

  def field_valid({:hcl, c}) do
    Regex.match?(~r/^#[0-9a-f]{6}$/, c)
  end

  def field_valid({:ecl, c}) do
    ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"] |> Enum.any?(&(&1 == c))
  end

  def field_valid({:pid, p}) do
    Regex.match?(~r/^[0-9]{9}$/, p)
  end

  def field_valid({:cid, _}) do
    true
  end
end
