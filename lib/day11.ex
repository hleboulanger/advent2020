defmodule Day11 do
  # inspired by https://rosettacode.org/wiki/Conway%27s_Game_of_Life#Elixir
  # for Conway Game of Life

  @filename "inputs/day11"

  def phase1(filename \\ @filename) do
    {board, size} = load_board(filename)

    # print_board(board, "", size, 0)
    {reason, board} = generate("", size, 100, board, 1)

    case reason do
      :static -> "no movement"
    end
    |> IO.puts()

    board |> Map.values() |> Enum.count(&(&1 == "#"))
  end

  def new_board(n) do
    for x <- 1..n, y <- 1..n, into: %{}, do: {{x, y}, "."}
  end

  def print_board(_, name, _, generation) do
    IO.puts("#{name}: generation #{generation}")

    # Enum.each(1..n, fn y ->
    #   Enum.map(1..n, fn x -> board[{x, y}] end)
    #   |> IO.puts()
    # end)
  end

  def load_board(filename) do
    data =
      File.read!(filename)
      |> String.trim()
      |> String.split()

    size = Enum.count(data)

    board =
      data
      |> Enum.with_index()
      |> Enum.reduce(new_board(size), fn {line, y}, acc ->
        String.graphemes(line)
        |> Enum.with_index()
        |> Enum.reduce(acc, fn {c, x}, acc ->
          %{acc | {x + 1, y + 1} => c}
        end)
      end)

    {board, size}
  end

  def cell_next_gen(board, x, y, n) do
    irange = max(1, x - 1)..min(x + 1, n)
    jrange = max(1, y - 1)..min(y + 1, n)

    seat = board[{x, y}]

    f =
      for(i <- irange, j <- jrange, do: board[{i, j}])
      |> Enum.reject(&(&1 == "."))
      |> Enum.frequencies()
      |> Map.update(seat, 0, &max(&1 - 1, 0))

    cond do
      seat == "." -> "."
      seat == "L" && Map.get(f, "#", 0) == 0 -> "#"
      seat == "#" && Map.get(f, "#", 0) >= 4 -> "L"
      true -> seat
    end
  end

  def evolve(board, n) do
    for x <- 1..n, y <- 1..n, into: %{}, do: {{x, y}, cell_next_gen(board, x, y, n)}
  end

  defp generate(_, _, generations, _, gen) when generations < gen, do: :ok

  defp generate(name, size, generations, board, gen) do
    new = evolve(board, size)
    # print_board(new, name, size, gen)

    cond do
      board == new -> {:static, new}
      true -> generate(name, size, generations, new, gen + 1)
    end
  end
end
