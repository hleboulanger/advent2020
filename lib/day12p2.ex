defmodule Day12P2 do
  @filename "inputs/day12"

  def load_program(filename) do
    File.read!(filename)
    |> String.trim()
    |> String.split("\n")
    |> Enum.map(&parse_line/1)
  end

  def parse_line(line) do
    {a, v} = String.split_at(line, 1)

    %{
      :action => a,
      :value => String.to_integer(v)
    }
  end

  @doc """
  iex> Day12P2.phase2("inputs/day12_example")
  286
  """
  def phase2(filename \\ @filename) do
    {{x, y}, _, _} =
      load_program(filename)
      |> Enum.reduce({{0, 0}, "E", {10, 1}}, &move/2)

    abs(x) + abs(y)
  end

  def move(%{action: "F", value: v}, {{bx, by}, dir, {x, y}}) do
    {{bx + x * v, by + y * v}, dir, {x, y}}
  end

  def move(%{action: "N", value: v}, {boat, dir, {x, y}}), do: {boat, dir, {x, y + v}}
  def move(%{action: "S", value: v}, {boat, dir, {x, y}}), do: {boat, dir, {x, y - v}}
  def move(%{action: "E", value: v}, {boat, dir, {x, y}}), do: {boat, dir, {x + v, y}}
  def move(%{action: "W", value: v}, {boat, dir, {x, y}}), do: {boat, dir, {x - v, y}}

  def move(%{action: "R", value: 0}, coords), do: coords

  def move(%{action: "R", value: v}, {boat, dir, {x, y}}),
    do: move(%{action: "R", value: v - 90}, {boat, dir, {y, -x}})

  def move(%{action: "L", value: 0}, coords), do: coords

  def move(%{action: "L", value: v}, {boat, dir, {x, y}}),
    do: move(%{action: "L", value: v - 90}, {boat, dir, {-y, x}})
end
