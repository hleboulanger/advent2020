defmodule Day1 do
  def phase1() do
    [x, y] =
      File.read!("inputs/day1")
      |> two_entries
      |> IO.inspect()

    x * y
  end

  def phase2() do
    [x, y, z] =
      File.read!("day1")
      |> three_entries
      |> IO.inspect()

    x * y * z
  end

  @doc """
  iex> Day1.two_entries("1721
  ...> 979
  ...> 366
  ...> 299
  ...> 675
  ...> 1456")
  [1721,299]
  """
  def two_entries(lines) do
    numbers =
      lines
      |> String.split()
      |> Enum.map(&String.to_integer(&1))

    try do
      for x <- numbers, y <- numbers, do: if(x + y == 2020, do: throw([x, y]), else: x)
    catch
      [x, y] -> [x, y]
    end
  end

  @doc """
  iex> Day1.three_entries("1721
  ...> 979
  ...> 366
  ...> 299
  ...> 675
  ...> 1456")
  [979,366,675]
  """
  def three_entries(lines) do
    numbers =
      lines
      |> String.split()
      |> Enum.map(&String.to_integer(&1))

    try do
      for x <- numbers,
          y <- numbers,
          z <- numbers,
          do: if(x + y + z == 2020, do: throw([x, y, z]), else: x)
    catch
      [x, y, z] -> [x, y, z]
    end
  end
end
